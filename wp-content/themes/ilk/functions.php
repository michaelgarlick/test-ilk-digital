<?php
// Initial theme/wordpress setup 
require('functions/_init.php');
// Add Scripts and Stylesheets
require('functions/scripts.php');
// Register Custom Post Types and Taxonomies
require('functions/custom-post-types.php');

/****
*
*	Custom functions below here....
*
****/