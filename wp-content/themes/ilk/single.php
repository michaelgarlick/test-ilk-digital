<!-- single.php -->
<?php get_header(); ?>

		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<header>
							<h1 class="page-title"><?php the_title(); ?></h1>
						</header>
					</div>
				</div>
			</div>
		</section>
		
		<main>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</main>

		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php comments_template(); ?> 
					</div>
				</div>
			</div>
		</section>
		
<?php get_footer(); ?>