<!-- template-example.php -->
<?php 
/*
Template Name: Template Example
*/
get_header(); ?>
		<section>
			<div>
				<div class="row">
					<div class="col-md-12">
						<header>
							<h1 class="page-title"><?php the_title(); ?></h1>
						</header>
					</div>
				</div>
			</div>
		</section>
		<main>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</main>
<?php get_footer(); ?>