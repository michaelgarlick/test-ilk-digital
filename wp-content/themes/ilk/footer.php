	<!-- Footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					&copy; Manifest <?php echo date('Y'); ?>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer();?>
	</body>
</html>