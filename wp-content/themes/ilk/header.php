<!doctype html>
<html>
	<head>
		<!-- META -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php if(is_front_page()): ?>

		<title><?php wp_title( ); ?></title>

	<?php else:?>

		<title><?php wp_title( ' | ', true, 'right' ); ?></title>

	<?php endif; ?>

		<?php 
			// Options -> SEO - Search Console
			if( function_exists('acf_add_options_page') ) {
				the_field('google_search_console_meta_code', 'option');
			}
		?>
		
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
		<!--[if lt IE 9]>
			<script src="<?php bloginfo('template_url'); ?>/js/header/html5shiv.js"></script>
			<script src="<?php bloginfo('template_url'); ?>/js/header/respond.min.js"></script>
		<![endif]-->

		<?php wp_head(); ?>

		<?php 
			// Options -> SEO -> Analytics
			if( function_exists('acf_add_options_page') ) {
				echo "\n<script>\n";
				the_field('google_analytics_code', 'option');
				echo "\n</script>";
			}
		?>

	</head>
	<body <?php body_class(); ?>>

		<!-- header -->
		<header id="header" role="heading">
			<div class="container">
				<div class="row">
				<div class="col-md-4">
					<div class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
					<?php	$description = get_bloginfo( 'description', 'display' );
						if ( $description || is_customize_preview() ) : ?>
							<p class="site-description"><?php echo $description; ?></p>
						<?php endif;
					?>
				</div>
					<div class="col-md-8">

						<?php 
							// Description walker class is in the functions.php file and can be activeted by uncommenting line below and walker line in $defaults
							//$walker = new Menu_With_Description;
							$defaults = array(
								'menu'            => '', // Name of Menu
								'container'       => 'nav',
								'container_class' => 'menu-{menu slug}-container',
								'container_id'    => '',
								'menu_class'      => 'menu',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id=\"%1$s\" class=\"%2$s\">%3$s</ul>',
								'depth'           => 0,
								//'walker' 		=> $walker
							);
							wp_nav_menu( $defaults );
						?>

					</div>
				</div>
			</div>
		</header>