<?php 
// function create_CPT() {  
//     register_post_type('example', // Register Custom Post Type
//         array(
//         'labels' => array(
//             'name' => __('Example'), // Rename these to suit
//             'singular_name' => __('Example'),
//             'add_new' => __('Add Example'),
//             'add_new_item' => __('Add Example'),
//             'edit' => __('Edit'),
//             'edit_item' => __('Edit'),
//             'new_item' => __('New Example'),
//             'view' => __('View Examples'),
//             'view_item' => __('View Example'),
//             'search_items' => __('Search Examples'),
//             'not_found' => __('No Examples found'),
//             'not_found_in_trash' => __('No Examples found in Trash')
//         ),
//         'public' => true,
//         'publicly_queryable' => true,
//         'exclude_from_search' => false,
//         'menu_icon' => 'dashicons-businessman', // ICONS: http://www.kevinleary.net/wordpress-dashicons-list-custom-post-type-icons/
//         'hierarchical' => true,
//         'has_archive' => true,
//         'supports' => array(
//         	// Remove as needed.
// 			'title',
// 			'editor',
// 			'author',
// 			'thumbnail',
// 			'excerpt',
// 			'trackbacks',
// 			'custom-fields',
// 			'comments',
// 			'revisions',
// 			'page-attributes',
// 			'post-formats'
//          )
//      ));
// } add_action('init', 'create_CPT');

// // Register Taxonomy for Custome Post Type
// function create_taxonomy() {
// 	register_taxonomy(
// 		'taxonomy-name',
// 		'example',
// 		array(
// 			'label' => __( 'Taxonomy Name' ),
// 			'rewrite' => array( 'slug' => 'taxonomy-slug' ),
// 			'hierarchical' => true,
// 		)
// 	);
// }
// add_action( 'init', 'create_taxonomy' );
?>