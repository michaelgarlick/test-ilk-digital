<?php
/**
 * REMOVE UNWANTED <HEAD> ITMES
 *
 * Removes verison info in src/href of items and gets rid of things like emojis.
 * and other user uneccicary stuff.
 *
 * 
 */
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'print_emoji_detection_script', 7 );
	remove_action('wp_print_styles', 'print_emoji_styles' );
	function remove_cssjs_ver( $src ) {
	    if( strpos( $src, '?ver=' ) )
	        $src = remove_query_arg( 'ver', $src );
	    return $src;
	}
	add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
	add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

/**
 * THEME SUPPORT 
 *
 * Adds theme support of sidebars, menus and for customising backend/wordpress.
 * 
 *
 * 
 */

	// Remove admin bar at frontend
	show_admin_bar( false );
	// Register Nav menus and Sidebars
	register_nav_menus();
	register_sidebars();
	add_theme_support( 'html5' );
	// Add theme supprt to posts
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio'
	));
	add_theme_support( 'custom-background' );
	add_theme_support( 'custom-header' );
	add_theme_support( 'custom-logo' );
	// Add admin CSS to login and wordpress backend.
	function admin_css() {
		echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/functions/admin.css" type="text/css" media="all" />';
	}
	add_action( 'login_enqueue_scripts', 'admin_css' );
	add_action('admin_head', 'admin_css');
	// add admin JS - optional
	add_action('admin_footer', 'custom_admin_js');
	function custom_admin_js() {
	    $url = get_bloginfo('template_directory') . '/js/wp-admin.js';
	    echo '"<script type="text/javascript" src="'.get_bloginfo('template_directory').'/functions/admin.js"></script>"';
	}

/**
 * WALKER CLASS FOR MENUS - OPTIONAL
 *
 * Add ability to add content to menu  <LI> items like descriptions. Implemented in template
 * with wp_nav_menu() (http://www.wpbeginner.com/wp-themes/how-to-add-menu-descriptions-in-your-wordpress-themes/)
 *
 * 
 */
	class Menu_With_Description extends Walker_Nav_Menu {
		function start_el(&$output, $item, $depth = 0, $args = array(), $id=0) {
			global $wp_query;
			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
			$class_names = $value = '';
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
			$class_names = ' class="' . esc_attr( $class_names ) . '"';

			$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

			$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
			$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
			$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';
			// EDIT $item_output to suit your needs...
			$item_output = $args->before;
			$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= '<br /><span class="sub">' . $item->description . '</span>';
			$item_output .= '</a>';
			$item_output .= $args->after;
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id );
		}
	}

/**
 * GZIP SUPPORT
 *
 * Adds Gzip to .htaccess if not present.
 * 
 *
 * 
 */
	function add_gzip(){
		if( !strpos(file_get_contents(".htaccess"),'gzip')) {
			$f = fopen('.htaccess', "a+");
			fwrite($f, "
	<ifModule mod_gzip.c>
		mod_gzip_on Yes
		mod_gzip_dechunk Yes
		mod_gzip_item_include file .(html?|txt|css|js|php|pl)$
		mod_gzip_item_include handler ^cgi-script$
		mod_gzip_item_include mime ^text/.*
		mod_gzip_item_include mime ^application/x-javascript.*
		mod_gzip_item_exclude mime ^image/.*
		mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
	</ifModule>
			");
			fclose($f);
	    }
	}
	add_action( 'after_setup_theme', 'add_gzip' );


function dummy_content() {
	echo '<h1>Header Level 1</h1> <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p> <h2>Header Level 2</h2> <ol> <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li> <li>Aliquam tincidunt mauris eu risus.</li> </ol> <blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote> <h3>Header Level 3</h3> <ul> <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li> <li>Aliquam tincidunt mauris eu risus.</li> </ul> <h4>Header Level 4</h4> <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p> <h5>Header Level 5</h5> <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p> <h6>Header Level 6</h6> <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p> <pre><code> #header h1 a {display: block; width: 300px; height: 80px; } </code></pre>';
}