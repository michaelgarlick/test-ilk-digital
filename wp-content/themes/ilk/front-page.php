<!-- Front Page -->
<?php get_header(); ?>
		<main>
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-6">
						
						<?php dummy_content(); // DELETE THIS ##### ?>
						<hr>
						<?php while ( have_posts() ) : the_post(); ?>
							<article>
								<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								<?php the_excerpt(); ?>
							</article>
						<?php endwhile; ?>
					</div>
					<aside class="col-md-3 col-sm-6">
						<?php get_sidebar(); ?>
					</aside>
				</div>
			</div>
		</main>
<?php get_footer(); ?>