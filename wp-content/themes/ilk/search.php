<!-- index.php -->
<?php get_header(); ?>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<header>
							<h1 class="page-title"><?php printf( __( 'Search Results for: %s' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
						</header>
					</div>
				</div>
			</div>
		</section>
		<main  role="main">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ol>
							<?php while ( have_posts() ) : the_post(); ?>
								<li>
									<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
									<?php the_excerpt(); ?>
								</li>
							<?php endwhile; ?>
						</ol>
						<?php 
							the_posts_pagination( array(
								'prev_text'          => __( 'Previous page', 'twentysixteen' ),
								'next_text'          => __( 'Next page', 'twentysixteen' ),
								'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
							) );
						?>
					</div>
				</div>
			</div>
		</main>
<?php get_footer() ?>