$(document).ready(function() {
	// SEO helper
	var H1_count = $('h1').length;
	if(H1_count != 1) {
		console.log('SEO TIP: there are '+H1_count+' on this page. For best practice SEO please have 1 H1 tag per page.');
	}

});