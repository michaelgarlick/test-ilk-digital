<!-- index.php -->
<?php get_header(); ?>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<header>
							<?php
								the_title();
								the_archive_description( '<div class="archive-description">', '</div>' );
							?>
						</header>
					</div>
				</div>
			</div>
		</section>
		<main role="main">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php while ( have_posts() ) : the_post(); ?>
							<article <?php post_class(); ?>>
								<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								<?php the_excerpt(); ?>
							</article>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</main>
<?php get_footer() ?>