
This blank theme was built for the purpose of building custom themes for our clients.

It includes the following:

- SASS css integration (so use codekit when developing locally - config.rb)
	- _booty.scss is the core grid system and doc reset with additional general styles
	- use _mixins.scss for additional style libraries
	- the _variables.scss is optional depending on what you want to do.
	- All theme CSS to go in style.scss

- Auto Plugin install for:
	
	- Advanced Custom Fields	
		- Theme Options sections included:
			- General: blank
			- Header: blank
			- Footer: blank
			- SEO: 2 pre configured fields for Google Analytics (JS) and Search Console (meta tag method)

	- Yoast SEO (configure for client before going live)
		- Link to Search console

	- Disable Comments - Optional

- Stripped and partitioned functions.php
	- See functions directory for individual components (use/delete as appropriate)
		- adding JS scripts without need to clog header.php
		- Theme support for:
			- Post formats
			- Thumbnails
			- Sidebars (add additional ones yourself)
			- Nav Menus
		- Gzip added to .htaccess if not present
		- Hide surplus <head> meta
		- Required theme plugins:
			- ACF
				- See above for pre defined option pages/fields
			- Yoast SEO
			- Disable Comments
		- Nav menu description walker
		- Custom post types (one example one included) & taxonomies
		- WP admin css 

- All required WordPress Template files.
	- Additional example template file (template-example.php)

- Use 'Appearance->Customize->Site Identity' to add favicon/site icon 512px by 512px






