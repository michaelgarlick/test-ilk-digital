<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OvJuTMBHAUs)6kUjnuT)KC<6t3VO%$]0l3h!@)y<.=,<vv A(;_RD8U NA3V^]:<');
define('SECURE_AUTH_KEY',  '`=,1KT{+o@*2;8A,Vc=1=CWZ_6sb-<^D$xPe)ol(?]2^zp1QU`iSaJ4yh1u.~F4Q');
define('LOGGED_IN_KEY',    '3@m>q:KpdEbYrA`:n)4)$9gPK_17,mPj,.1xvYapi2iPVliXYtxn;8BL&.SAyiT.');
define('NONCE_KEY',        'FnOiVVis/}EP9(]y<yw?jXaoCec`Ue_eXpr lcO=oj:OWOb|1=fLH9+RX/t5[PzF');
define('AUTH_SALT',        '6V<{0%u!::bxslOFMCb)TvfaKJWQ71iNb|QzW;Y-H-&yBi}Dw?h4~oyUjOOkZj_!');
define('SECURE_AUTH_SALT', '`3q}am3=/O-3?rN2$W|_Lt7Pw>v-ljI(/qn$DEC3`%+EY^&d|iTGni7m~t0hcdP<');
define('LOGGED_IN_SALT',   'PksCh3_ZZq6EbEV~=DbXZ_!~81Gw^f,n)jssBw+j@fgP0_g w:y)2i}ujl_f mg(');
define('NONCE_SALT',       '#,}Aq6C<%$4RP7(O!E]~#.]^F:f`jx($S+P~NG .OFwMp)9-A!>>{l0&|H5JxOy&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'test_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
